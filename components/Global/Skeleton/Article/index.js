import { Card, Row, Col } from 'react-bootstrap'
import { Skeleton } from 'antd'

const ArticleSkeleton = () => {
  return (
    <Card className="border-0">
      <Row>
        <Col xs={1}>
          <Skeleton paragraph={false} active/>
          <Skeleton paragraph={false} active/>
        </Col>
        <Col xs={11}>
          <Skeleton paragraph={{ rows: 2 }} active/>
        </Col>
      </Row>
    </Card>
  )
}

export default ArticleSkeleton
