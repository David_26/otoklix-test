import { useState, useEffect } from 'react'
import CKEditor from 'ckeditor4-react'

const TextEditor = ({ value, editorValue }) => {
  const [content, setContent] = useState(null)

  const handleChangeEditorId = (val) => {
    setContent(val)
    editorValue(val)
  }

  useEffect(() => {
    if (value) {
      setContent(value)
    }
  },[value])

  return (
    <div>
      <CKEditor
        name="content"
        data={content}
        config={{
          extraPlugins: 'justify,font,colorbutton',
          toolbarGroups: [
            { name: 'basicstyles', groups: ['basicstyles', 'cleanup'] },
            { name: 'styles' },
            { name: 'links' },
            {
              name: 'paragraph',
              groups: ['list', 'indent', 'blocks', 'align', 'bidi'] // 'align' -> 'justify' plugin
            }
          ],
          removeButtons: 'Styles'
        }}
        onChange={e => handleChangeEditorId(e.editor.getData())}
      />
    </div>
  )
}

export default TextEditor
