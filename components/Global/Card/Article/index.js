import React from 'react'
import { useRouter } from 'next/router'
import { Row, Col, Card } from 'react-bootstrap'
import styles from './Article.module.scss'
import dayjs from 'dayjs'
import { Typography, Button, Popconfirm, Divider } from 'antd'
import { EditOutlined, DeleteOutlined } from '@ant-design/icons'

const { Paragraph } = Typography

const ArticleCard = ({
  id, date, title,
  description, deleteArticle, deleteLoading
}) => {
  const router = useRouter()

  const handleDelete = () => {
    deleteArticle(id)
  }

  return (

    <Card className={`border-0 ${styles['articles-card']}`}>
      <Row>
        <Col lg={2} className="d-none d-lg-block">
          <div className={styles['date-section']}>
            <h5>{dayjs(date).format('DD MMM') || '-'}</h5>
            <h6>{dayjs(date).format('YYYY') || '-'}</h6>
          </div>
        </Col>
        <Col xs={12} lg={10}>
          <div className={`mx-3 ${styles['content-section']}`}>
            <h3 className="font-weight-bold">{title}</h3>
            <p>{dayjs(date).format('DD MMM YYYY') || '-'}</p>
            <Paragraph ellipsis={{rows: 2}}>
              <p dangerouslySetInnerHTML={{__html: description}}></p>
            </Paragraph>
            <div className="d-flex justify-content-start">
              <Button type="primary" onClick={() => router.push(`/article/${id}`)} className="mr-1 mr-lg-2">Read More</Button>
              <Button type="primary" className="mr-1 mr-lg-2" icon={<EditOutlined/>} onClick={() => router.push(`/article/edit/${id}`)}>Edit</Button>
              <Popconfirm
                title="Are you sure delete this article?"
                okText="Yes"
                cancelText="No"
                placement="topLeft"
                onConfirm={() => handleDelete(id)}
              >
                <Button type="primary" className="mr-3" icon={<DeleteOutlined/>} danger disabled={deleteLoading}>Delete</Button>
              </Popconfirm>
            </div>
          </div>
          <Divider className="my-4"/>
        </Col>
      </Row>
    </Card>
  )
}

export default ArticleCard
