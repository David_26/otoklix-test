import { Button, DatePicker, version } from "antd"

const ContentGlobal = () => {
  return (
    <div className="content-container">
      <h1>antd version: {version}</h1>
      <DatePicker />
      <Button type="primary" style={{ marginLeft: 8 }}>
        Primary Button
      </Button>
    </div>
  )
}

export default ContentGlobal
