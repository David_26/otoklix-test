import { Navbar, Nav, Container } from 'react-bootstrap'

const HeaderComponent = () => {
  return (
    <Navbar collapseOnSelect expand="lg" bg="dark" variant="dark">
      <Container>
        <Navbar.Brand href="/">Otoklix Test</Navbar.Brand>
        <Nav className="ml-auto">
          <Nav.Link href="/article/add">Add Article</Nav.Link>
        </Nav>
      </Container>
    </Navbar>
  )
}

export default HeaderComponent
