import { Layout } from 'antd'
import styles from './Footer.module.scss'
const { Footer } = Layout

const FooterComponent = () => {
  return (
    <>
      <Footer>
        <p className={styles['footer-wording']}>Copyright 2021</p>
      </Footer>
    </>
  )
}

export default FooterComponent
