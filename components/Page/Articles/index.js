import React, { useState, useEffect } from 'react'
import { Empty, notification } from 'antd'
import { CheckCircleOutlined } from '@ant-design/icons'
import { Container, Row, Col } from 'react-bootstrap'
import { apiGetNonAuth, apiDeleteAuth } from '~/utils/api'
import { API_ARTICLE } from '~/utils/api-url'
import ArticleCard from '~/components/Global/Card/Article'
import ArticleSkeleton from '~/components/Global/Skeleton/Article'
import { motion } from 'framer-motion'

const Articles = () => {
  const [isDelete, setIsDelete] = useState(false)
  const [data, setData] = useState({
    data: {},
    isFetching: true
  })

  const variants = {
    hidden: { opacity: 0, x: -200, y: 0 },
    enter: { opacity: 1, x: 0, y: 0 },
    exit: { opacity: 0, x: 0, y: -100 }
  }

  const getArticles = async () => {
    try {
      const res = await apiGetNonAuth(API_ARTICLE.GET_ALL_CONTENT)
      if (res.data) {
        setData({
          data: res.data,
          isFetching: false
        })  
        
      } else {
        setData({
          data: {},
          isFetching: false
        })
      }
    } catch(err) {
      setData({
        data: {},
        isFetching: false
      })
    }
  }

  const handleDelete = async (id) => {
    setIsDelete(true)
    try {
      const res = await apiDeleteAuth(API_ARTICLE.DELETE_CONTENT(id))
      if (res.status === 200) {
        notification.open({
          message: 'Delete Success',
          description: 'Article has been deleted',
          icon: <CheckCircleOutlined style={{ color: '#108ee9' }} />
        })
        setIsDelete(false)
        getArticles()
      }
    } catch(err) {
      setIsDelete(false)
      notification.open({
        message: 'Oops!',
        description: 'Something when wrong'
      })
    }
  }

  useEffect(() => {
    getArticles()
  }, [])

  return(
    <Container>
      <Row>
        {data.isFetching ?
          [1,2,3].map(index => (
            <Col key={index} lg={12} className="mb-3">
              <ArticleSkeleton/>
            </Col>
          ))
          :
          data.data.length > 0 ?
            data.data.map(item => (
              <Col key={item.id} lg={12}>
                <motion.div variants={variants} initial="hidden" animate="enter" exit="exit" transition={{ type: 'linear' }} >
                  <ArticleCard
                    id={item.id}
                    date={item.published_at}
                    title={item.title}
                    description={item.content}
                    deleteArticle={(id) => handleDelete(id)}
                    deleteLoading={isDelete}
                  />
                </motion.div>
              </Col>
            ))
            :
            <Col>
              <Empty/>
            </Col>
        }
      </Row>
    </Container>
  )
}

export default Articles
