import React from 'react'
import { useRouter } from 'next/router'
import { Container, Row, Col } from 'react-bootstrap'
import { PREFIX_IMAGE } from '~/utils/constant'
import { Skeleton, Button, Popconfirm, Empty } from 'antd'
import dayjs from 'dayjs'
import { LeftOutlined, EditOutlined, DeleteOutlined } from '@ant-design/icons'
import { isEmpty } from 'lodash'
import { motion } from 'framer-motion'

const ArticleDetail = ({ data, deleteArticle, deleteLoading, isFetching }) => {
  const router = useRouter()

  const variants = {
    hidden: { opacity: 0, x: -200, y: 0 },
    enter: { opacity: 1, x: 0, y: 0 },
    exit: { opacity: 0, x: 0, y: -100 }
  }

  const handleDelete = (id) => {
    deleteArticle(id)
  }

  return(
    <Container>
      <Row>
        <Col xs={12} lg={8} className="mx-auto">
          {isFetching ?
            <Skeleton paragraph={{ rows: 5 }} active/>
            :
            !isEmpty(data) ?
              <motion.div variants={variants} initial="hidden" animate="enter" exit="exit" transition={{ type: 'linear' }} >
                <div className="mb-3 d-flex justify-content-between">
                  <Button icon={<LeftOutlined/>} onClick={() => router.push(`/`)}>Go back</Button>
                  <div>
                    <Button type="primary" className="mr-1 mr-lg-2" icon={<EditOutlined/>} onClick={() => router.push(`/article/edit/${data.id}`)}>Edit</Button>
                    <Popconfirm
                      title="Are you sure delete this article?"
                      okText="Yes"
                      cancelText="No"
                      placement="topLeft"
                      onConfirm={() => handleDelete(data.id)}
                    >
                      <Button type="primary" icon={<DeleteOutlined/>} danger loading={deleteLoading}>Delete</Button>
                    </Popconfirm>
                  </div>
                </div>
                <div className="mb-4">
                  <img src={`${PREFIX_IMAGE}/desk.jpg`} className="img-fluid" alt="bg"/>
                </div>
                <div className="mb-5">
                  <h3>{data.title || '-'}</h3>
                  <h6>Published at: {dayjs(data.published_at).format('DD MMM YYYY') || '-'}</h6>
                </div>
                <div>
                  <p className="article-paragraph text-justify" dangerouslySetInnerHTML={{__html: data.content || '-'}}></p>
                </div>
              </motion.div>
              :
              <Empty/>
          }
        </Col>
      </Row>
    </Container>
  )
}

export default ArticleDetail
