import React, { useState, useEffect } from 'react'
import { useRouter } from 'next/router'
import { Container, Row, Col } from 'react-bootstrap'
import { Button, Form, Input } from 'antd'
import TextEditor from '~/components/Global/TextEditor'
import { motion } from 'framer-motion'

const ArticleForm = ({ data, submit, isSubmit }) => {
  const router = useRouter()
  const [form] = Form.useForm()
  const [contentEditor, setContentEditor] = useState(null)

  const variants = {
    hidden: { opacity: 0, x: -200, y: 0 },
    enter: { opacity: 1, x: 0, y: 0 },
    exit: { opacity: 0, x: 0, y: -100 }
  }

  const handleSubmit = (values) => {
    submit(values)
  }

  useEffect(() => {
    if (data) {
      setContentEditor(data.content) //initial value ckeditor
      form.setFieldsValue({
        title: data.title || '',
        content: data.content || ''
      })
    }
  }, [data])

  const handleUpdateEditor = (val) => { //onchange and update form.content from ckeditor
    form.setFieldsValue({
      content: val
    })
  }

  return(
    <Container>
      <motion.div variants={variants} initial="hidden" animate="enter" exit="exit" transition={{ type: 'linear' }} >
        <Form
          name="article-form"
          onFinish={handleSubmit}
          form={form}
        >
          <Row>
            <Col xs={12} lg={8} className="mx-auto">
              <label>Title</label>
              <Form.Item name="title" rules={[{ required: true }]}>
                <Input placeholder="Title" />
              </Form.Item>
            </Col>
          </Row>
          <Row>
            <Col xs={12} lg={8} className="mx-auto">
              <label>Content</label>
              <Form.Item name="content" rules={[{ required: true }]}>
                <TextEditor value={contentEditor} editorValue={(val) => handleUpdateEditor(val)}/>
              </Form.Item>
            </Col>
          </Row>
          <Row>
            <Col xs={12} lg={8} className="mx-auto">
              <div className="d-flex justify-content-end">
                <Button onClick={() => router.back()} className="mr-2">Cancel</Button>
                <Button type="primary" htmlType="submit" loading={isSubmit}>Submit</Button>
              </div>
            </Col>
          </Row>
        </Form>
      </motion.div>
    </Container>
  )
}

export default ArticleForm
