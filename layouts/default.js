import React from 'react'
import Head from 'next/head'
import { Layout } from 'antd'
import Header from '~/components/Header'
import Footer from '~/components/Footer'
const { Content } = Layout

const LayoutComponent = (props) => (
  <>
    <Head>
      <title>{props.meta_title}</title>
      <meta name="title" content={props.meta_title} key="title" />
      <meta name="description" content={props.meta_description} key="desc" />
      <meta name="og:title" content={props.meta_title} key="ogtitle" />
      <meta name="og:description" content={props.meta_description} key="ogdesc" />
      <meta name="og:type" content={props.meta_type} key="ogtype" />
    </Head>
    <Header />
    <Content className="my-5">
      {props.children}
    </Content>
    <Footer />
  </>
)

export default LayoutComponent
