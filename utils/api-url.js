/*eslint-env node*/
// const BASE_URL = `${process.env.BASE_URL}/api`

// Home
// export const API_HOME = {
//   CONTENT_ONE: `${BASE_URL}/content_1`,
//   CONTENT_TWO: (id) => `${BASE_URL}/content_2/${id}`
// }

export const API_ARTICLE = {
  GET_ALL_CONTENT: `https://limitless-forest-49003.herokuapp.com/posts`,
  GET_DETAIL_CONTENT: (id) => `https://limitless-forest-49003.herokuapp.com/posts/${id}`,
  ADD_CONTENT: `https://limitless-forest-49003.herokuapp.com/posts`,
  UPDATE_CONTENT: (id) => `https://limitless-forest-49003.herokuapp.com/posts/${id}`,
  DELETE_CONTENT: (id) => `https://limitless-forest-49003.herokuapp.com/posts/${id}`
}
