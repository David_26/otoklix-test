import axios from 'axios'
import { METHOD } from './constant'
import { message } from 'antd'

axios.interceptors.response.use(
  (response) => {
    return response
  },
  (error) => {
    if (error.response) {
      const { data } = error.response
      if (data.meta && data.meta.message) {
        message.error(data.meta.message)
      } else if (data && data.message) {
        message.error(data.message)
      } else {
        message.error(error.message)
      }
    }
    return Promise.reject(error)
  }
)

export const apiGetNonAuth = (URL, params) =>
  axios({
    url: URL,
    method: METHOD.GET,
    params
  })
export const apiGetAuth = (URL, params) =>
  axios({
    url: URL,
    method: METHOD.GET,
    params,
    headers: {
      Authorization: ''
    }
  })

export const apiPostNonAuth = (URL, data) =>
  axios({
    url: URL,
    method: METHOD.POST,
    data
  })
export const apiPostAuth = (URL, data) =>
  axios({
    url: URL,
    method: METHOD.POST,
    data,
    headers: {
      Authorization: ''
    }
  })


export const apiPutNonAuth = (URL, data) =>
  axios({
    url: URL,
    method: METHOD.PUT,
    data
  })
export const apiPutAuth = (URL, data) =>
  axios({
    url: URL,
    method: METHOD.POST,
    data,
    headers: {
      Authorization: ''
    }
  })

export const apiPatchAuth = (URL, data) =>
  axios({
    url: URL,
    method: METHOD.PATCH,
    data,
    headers: {
      Authorization: ''
    }
  })

export const apiDeleteAuth = (URL, data) =>
  axios({
    url: URL,
    method: METHOD.DELETE,
    data,
    headers: {
      Authorization: ''
    }
  })
