import React from 'react'
import Layout from '~/layouts/default'
import Articles from '~/components/Page/Articles'

const Pages = () => {

  return (
    <>
      <Layout
        meta_title='Otoklix Test'
        meta_description='Otoklix Test'
      >
        <Articles />
      </Layout>
    </>
  )
}

export default Pages
