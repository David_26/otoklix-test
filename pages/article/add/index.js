import React, { useState } from 'react'
import Layout from '~/layouts/default'
import { useRouter } from 'next/router'
import { notification } from 'antd'
import { CheckCircleOutlined } from '@ant-design/icons'
import Form from '~/components/Page/Articles/Form'
import { apiPostNonAuth } from '~/utils/api'
import { API_ARTICLE } from '~/utils/api-url'

const AddArticle = () => {
  const router = useRouter()
  const [isSubmit, setIsSubmit] = useState(false)

  const handleSubmit = async (payload) => {
    setIsSubmit(true)

    try {
      const res = await apiPostNonAuth(API_ARTICLE.ADD_CONTENT, payload)
      if (res) {
        notification.open({
          message: 'Success',
          description: 'Article has been created',
          icon: <CheckCircleOutlined style={{ color: '#108ee9' }} />
        })
        setIsSubmit(false)
        router.push(`/`)
      }
    } catch(err) {
      setIsSubmit(false)
      notification.open({
        message: 'Oops!',
        description: 'Something when wrong'
      })
    }
  }

  return (
    <>
      <Layout
        meta_title="Add Article"
        meta_description="Add Article"
        meta_type="article"
      >
        <div className="page-wrapper pb-0 d-flex flex-column">
          <Form submit={(values) => handleSubmit(values)} isSubmit={isSubmit}/>
        </div>
      </Layout>
    </>
  )
}



export default AddArticle
