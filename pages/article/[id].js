import React, { useEffect, useState } from 'react'
import Layout from '~/layouts/default'
import { useRouter } from 'next/router'
import Detail from '~/components/Page/Articles/Detail'
import { apiDeleteAuth } from '~/utils/api'
import { API_ARTICLE } from '~/utils/api-url'
import { notification } from 'antd'
import { CheckCircleOutlined } from '@ant-design/icons'

const DetailArticle = ({ dataDetail }) => {
  const router = useRouter()
  const [isDelete, setIsDelete] = useState(false)
  const [isFetching, setIsFetching] = useState(true)

  useEffect(() => {
    if (dataDetail) {
      setIsFetching(false)
    }
  },[dataDetail])

  const handleDelete = async (id) => {
    setIsDelete(true)
    try {
      const res = await apiDeleteAuth(API_ARTICLE.DELETE_CONTENT(id))
      if (res.status === 200) {
        notification.open({
          message: 'Delete Success',
          description: 'Article has been deleted',
          icon: <CheckCircleOutlined style={{ color: '#108ee9' }} />
        })
        setIsDelete(false)
        router.push('/')
      }
    } catch(err) {
      notification.open({
        message: 'Oops!',
        description: 'Something when wrong'
      })
    }
  }

  return (
    <>
      <Layout
        meta_title={`Otoklix | ${dataDetail.title}` || '-'}
        meta_description={`${dataDetail.description}` || '-'}
        meta_type="article"
      >
        <div className="page-wrapper pb-0 d-flex flex-column">
          <Detail data={dataDetail} isFetching={isFetching} deleteArticle={(id) => handleDelete(id)} deleteLoading={isDelete}/>
        </div>
      </Layout>
    </>
  )
}



export default DetailArticle

export const getServerSideProps = async (context) => {
  const { id } = context.query
  let dataDetail = null
  await fetch(
    `https://limitless-forest-49003.herokuapp.com/posts/${id}`,
    {
      method: "GET"
    }
  )
    .then((response) => response.json())
    .then((json) =>{
      dataDetail = json
    })
    .catch(() => { 
      dataDetail = {}
    })

  return {
    props: {
      dataDetail
    }
  }
}
