import React, { useState } from 'react'
import Layout from '~/layouts/default'
import { useRouter } from 'next/router'
import { notification } from 'antd'
import Form from '~/components/Page/Articles/Form'
import { apiPutNonAuth } from '~/utils/api'
import { API_ARTICLE } from '~/utils/api-url'
import { CheckCircleOutlined } from '@ant-design/icons'

const DetailArticle = ({ detail }) => {
  const router = useRouter()
  const [isSubmit, setIsSubmit] = useState(false)

  const handleUpdate = async (payload) => {
    setIsSubmit(true)

    try {
      await apiPutNonAuth(API_ARTICLE.UPDATE_CONTENT(detail.id), payload)
      notification.open({
        message: 'Update Success',
        description: 'Article has been updated',
        icon: <CheckCircleOutlined style={{ color: '#108ee9' }} />
      })
      setIsSubmit(false)
      router.push(`/article/${detail.id}`)
    } catch(err) {
      setIsSubmit(false)
      notification.open({
        message: 'Oops!',
        description: 'Something when wrong'
      })
    }
  }

  return (
    <>
      <Layout
        meta_title={`${detail.title}` || '-'}
        meta_description={`${detail.description}` || '-'}
        meta_type="article"
      >
        <div className="page-wrapper pb-0 d-flex flex-column">
          <Form data={detail} submit={(values) => handleUpdate(values)} isSubmit={isSubmit}/>
        </div>
      </Layout>
    </>
  )
}



export default DetailArticle

export const getServerSideProps = async (context) => {
  const { id } = context.query
  let detail = null
  await fetch(
    `https://limitless-forest-49003.herokuapp.com/posts/${id}`,
    {
      method: "GET"
    }
  )
    .then((response) => response.json())
    .then((json) =>{
      detail = json
    })
    .catch(() => { 
      detail = {}
    })

  return {
    props: {
      detail
    }
  }  
}
